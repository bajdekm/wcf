﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLibrary
{
    [DataContract]
    public class BookIDExistException
    {
            private string _message;

            public BookIDExistException(string message)
            {
                _message = message;
            }

            [DataMember]
            public string Message { get { return _message; } set { _message = value; } }
      }
}
