﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {

       static List<Book> books = new List<Book>();
       static List<User> users = new List<User>();
       public Service1()
       {

       }

        public List<Book> listOfBorrowedBooks()
        {
            List<Book> borrowedBookList = new List<Book>();
            foreach (Book book in books)
            {
                if (book.bookInfo.borrowed)
                {
                    borrowedBookList.Add(book);
                }
            }
            return borrowedBookList;
        }

        public List<Book> getBorrowedBooksForUser(int userID)
        {
            List<Book> borrowedBookList = new List<Book>();
            foreach (Book book in books)
            {
                if (book.bookInfo.borrowedBy == userID)
                {
                    borrowedBookList.Add(book);
                }
            }
            return borrowedBookList;
        }

        public BookInfo getBookInfo(int bookID)
        {
            foreach (Book book in books)
            {
                if (book.bookID == bookID)
                {
                    return book.bookInfo;
                }
            }
            throw new FaultException<BookNotFoundException>(new BookNotFoundException("Book with id:" + bookID + " is not found in data base!"));
        }

        public string statusBorrowBook(int bookID)
        {
            foreach (Book book in books)
            {
                if (book.bookID == bookID)
                {
                    if (book.bookInfo.borrowed)
                        return "Book with id:" + bookID + " is borrowed!";
                    else
                        return "Book with id:" + bookID + " is not borrowed!";
                }
            }
            throw new FaultException<BookNotFoundException>(new BookNotFoundException("Book with id:" + bookID + " is not found in data base!"));
        }

        public void addNewBook(int bookID, string author, string title)
        {
            bool exception = false;
            foreach (Book book in books)
            {
                if (book.bookID == bookID)
                {
                    exception = true;
                    break;
                }
                else
                {
                    exception = false;
                }

            }
            if (exception)
            {
                throw new FaultException<BookIDExistException>( new BookIDExistException("Book id:" + bookID + " is already exist in data base!"));
            }

            books.Add(new Book(bookID, new BookInfo(author, title))); ;
        }

        public void borrowBookForUser(int userID, int bookID)
        {
            Book selectedBook = null;
            bool exception = false;
            foreach (Book book in books)
            {
                if (book.bookID == bookID)
                {
                    if (book.bookInfo.borrowed)
                    {
                        throw new FaultException<BookAlreadyBorrowedException>(new BookAlreadyBorrowedException("Book with id:" + bookID + " is already borrowed for other user!"));
                    }
                     selectedBook = book;
                    exception = false;
                    break;
                }
                else
                {
                    exception = true;
                }
            }
            if (exception)
            {
                throw new FaultException<BookNotFoundException>(new BookNotFoundException("Book with id:" + bookID + " is not found in data base!"));
            }
            foreach (User user in users)
            {
                if (user.userID == userID)
                {
                    exception = false;
                    break;
                }
                else
                {
                    exception = true;
                }
            }
            if (exception)
            {
                throw new FaultException<UserNotFoundException>(new UserNotFoundException("User with id:" + userID + " is not found in data base!"));
            }
            selectedBook.bookInfo.borrowed = true;
            selectedBook.bookInfo.borrowedBy = userID;

        }

        public List<Book> listOfBooks()
        {
            return books;
        }

        public void addNewUser(string name, string surname, int userID) 
        {
            bool exception = false;
            foreach (User user in users)
            {
                if (user.userID == userID)
                {
                    exception = true;
                    break;
                }
                else
                {
                    exception = false;
                }
            }
            if (exception)
            {
                throw new FaultException<UserIDExistException> (new UserIDExistException("User id:" + userID + " is already exist in data base!"));
            }
            User userToAdd = new User(userID, name, surname);
            users.Add(userToAdd);
        }


        public List<User> listOfusers()
        {
            return users;
        }

        public int getUsersCount()
        {
            int temp = users.Count();
            return temp;
        }

        public int getBooksCount()
        {
            int temp = books.Count();
            return temp;
        }
    }
}