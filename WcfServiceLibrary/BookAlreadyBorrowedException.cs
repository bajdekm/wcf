﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WcfServiceLibrary
{
    [DataContract]
    class BookAlreadyBorrowedException
    {
        private string _message;

        public BookAlreadyBorrowedException(string message)
            {
                _message = message;
            }

            [DataMember]
            public string Message { get { return _message; } set { _message = value; } }
    }
}
