﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication.ServiceReference1;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {

            Service1Client cc = new Service1Client();

            if (cc.getUsersCount() == 0) {
                cc.addNewUser("Jan", "Kowalski", 1);
                cc.addNewUser("Dratewka", "Szewczyk", 2);
                cc.addNewUser("Elton", "John", 3);

            }

            if (cc.getBooksCount() == 0) {
                cc.addNewBook(1, "Adam Mickiewicz", "Pan Tadeusz");
                cc.addNewBook(2, "Sienkiewicz", "W pustyni i w puszczy");
            }

            
            

            Console.WriteLine(aboutBook(cc.getBookInfo(1)));

            //cc.getBookInfo(7);

            //cc.addNewUser("Kolejny","User",2);
            
        }

        public static String aboutBook(BookInfo bi)
        {
            return "title: " + bi.title + " author: " + bi.author + " is available: " + bi.borrowed;
        }
    }
}
