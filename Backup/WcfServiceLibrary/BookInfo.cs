﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WcfServiceLibrary
{
    [DataContract]
    public class BookInfo
    {
        [DataMember]
        public bool borrowed { get; set; }
        [DataMember]
        public int borrowedBy { get; set; }
        [DataMember]
        public string author { get; set; }
        [DataMember]
        public string title { get; set; }

        public BookInfo(string author, string title)
        {

            this.author = author;
            this.title = title;
            this.borrowed = false;
        }
    }
}
