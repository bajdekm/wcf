﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        List<Book> listOfBorrowedBooks();

        [OperationContract]
        [FaultContract(typeof(UserNotFoundException))]
        List<Book> getBorrowedBooksForUser(int userID);

        [OperationContract]
        [FaultContract(typeof(BookNotFoundException))]
        BookInfo getBookInfo(int bookID);

        [OperationContract]
        [FaultContract(typeof(BookNotFoundException))]
        string statusBorrowBook(int bookID);

        [OperationContract]
        [FaultContract(typeof(BookIDExistException))]
        void addNewBook(int bookID, string author, string title);

        [OperationContract]
        [FaultContract(typeof(UserNotFoundException))]
        [FaultContract(typeof(BookNotFoundException))]
        [FaultContract(typeof(BookAlreadyBorrowedException))]
        void borrowBookForUser(int userID, int bookID);

        [OperationContract]
        List<Book> listOfBooks();

        [OperationContract]
        List<User> listOfusers();
     

        [OperationContract]
        [FaultContract(typeof(UserIDExistException))]
        void addNewUser(string name, string surname, int userID);
    }
}
