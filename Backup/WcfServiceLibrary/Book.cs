﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WcfServiceLibrary
{
    [DataContract]
    public class Book
    {
        [DataMember]
        public int bookID { get; set; }
        [DataMember]
        public DateTime returnDate { get; set; }
        [DataMember]
        public BookInfo bookInfo { get; set; }

        public Book(int bookID, BookInfo bookInfo)
        {

            this.bookID = bookID;
            this.bookInfo = bookInfo;
        }
    }
}
